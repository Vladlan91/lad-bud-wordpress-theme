<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package solid
 */

?>

	</div><!-- #content -->

	<div id="footerwrap">
		 	<div class="container">
			 	<div class="row">
			 		<h3 class="svg-text">LAD / BUD</h3>
			 		<div class="col-lg-4">
			 			<h4>LAD/BUD</h4>
			 			<div class="hline-w"></div>
			 			<p>
Успішність роботи нашої компанії обумовлена ​​кадровим і керівним потенціалом, наявністю фінансових і матеріальних ресурсів, що забезпечує якісне виконання ТОВ «Ладижинський Будівник» своїх зобов'язань перед партнерами</p>
			 		</div>
			 		<div class="col-lg-4">
			 			<h4>Соціальні мережі</h4>
			 			<div class="hline-w"></div>
			 			<p>
			 				<a href="#"><i class="fa fa-dribbble"></i></a>
			 				<a href="#"><i class="fa fa-facebook"></i></a>
			 				<a href="#"><i class="fa fa-twitter"></i></a>
			 				<a href="#"><i class="fa fa-instagram"></i></a>
			 				<a href="#"><i class="fa fa-tumblr"></i></a>
			 			</p>
			 		</div>
			 		<div class="col-lg-4">
			 			<h4>Наша адреса</h4>
			 			<div class="hline-w"></div>
			 			<p>
			 				Some Ave, 987,<br/>
			 				23890, New York,<br/>
			 				United States.<br/>
			 			</p>
			 		</div>
			 	
			 	</div><! --/row -->
			 	<div class="row">
			 		<div class="col-lg-12">
			 			<p>Lorem Ipsum i 2017</p>
			 		</div>
			 	</div><! --/row -->
		 	</div><! --/container -->
		 </div><! --/footerwrap -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
