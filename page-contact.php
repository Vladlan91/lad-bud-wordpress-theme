<?php
/**
 * Template name: Contact page 
 */

get_header();?>
<div id="blue">
	    <div class="container">
			<div class="row">
					<?php the_title( '<h3 class="entry-title">', '</h3>' ); ?>
			</div>
	    </div> 
	</div>
	<div class="container mtb">
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
